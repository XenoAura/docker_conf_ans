import os
import unittest
import requests
import json

from requests import Request
from random import choice

from config import HOST, USERNAME, PASSWORD

DEFAULT_HEADER = 'application/json'

SUCCESS = 200
BAD_REQUEST = 400
NO_CONTENT = 204

TEST_SPACE_KEY = "TEST"
TEST_CONTENT_NAME = "Test content title"


class TestRestApiConfluence(unittest.TestCase):
    test_content_id = None

    def _request(self, r_type, api_url, payload=None):
        headers = {'content-type': DEFAULT_HEADER}
        if payload:
            payload = json.dumps(payload)
        url = HOST + api_url
        resp = requests.request(method=r_type, url=url, data=payload, headers=headers, auth=(USERNAME, PASSWORD))
        try:
            return resp.status_code, resp.json()
        except ValueError:
            # DELETE return non JSON response
            return resp.status_code, ''

    def test_1_auth(self):
        url = '/rest/api/user/current'
        status_code, text = self._request('GET', url)
        self.assertEqual(text['username'], USERNAME)

    def test_2_create_space(self):
        url = '/rest/api/space'
        payload = {
            "key": TEST_SPACE_KEY,
            "name": "Test space " + TEST_SPACE_KEY,
            "type": "global",
            "description":
                {
                    "plain": {
                        "value": "Test space '{}' description".format(TEST_SPACE_KEY),
                        "representation": "plain"
                    }
                }
        }
        status_code, text = self._request('POST', url, payload)
        if 'already exists' not in str(text) and status_code == BAD_REQUEST:
            self.assertEqual(text["key"], TEST_SPACE_KEY)

    def test_3_create_page(self):
        url = '/rest/api/content/'
        payload = {"type": "page",
                   "title": TEST_CONTENT_NAME,
                   "space": {"key": TEST_SPACE_KEY},
                   "body":
                       {
                           "storage":
                               {
                                   "value": "Test content page content",
                                   "representation": "storage"
                               }
                       }
                   }

        status_code, text = self._request('POST', url, payload)
        if 'already exists' not in str(text) and status_code == BAD_REQUEST:
            self.assertEqual(text["title"], TEST_CONTENT_NAME)

    def test_4_get_page(self):
        url = '/rest/api/content?spaceKey={}&title={}'.format(TEST_SPACE_KEY, TEST_CONTENT_NAME)
        status_code, text = self._request('GET', url)
        self.assertEqual(text['results'][0]['title'], TEST_CONTENT_NAME)
        self.__class__.test_content_id = text['results'][0]['id']

    def test_5_update_content(self):
        url = '/rest/api/content/' + str(self.__class__.test_content_id)
        payload = {
            "id": self.__class__.test_content_id,
            "type": "page",
            "title": TEST_CONTENT_NAME,
            "space": {"key": TEST_SPACE_KEY},
            "body":
                {
                    "storage":
                        {
                            "value": "Updated content",
                            "representation": "storage"
                        }
                },
            "version":
                {"number": 2}
        }

        status_code, text = self._request('PUT', url, payload)
        self.assertEqual(status_code, SUCCESS)

    def test_6_delete_content(self):
        url = '/rest/api/content/' + str(self.__class__.test_content_id)
        status_code, text = self._request('DELETE', url)
        self.assertEqual(status_code, NO_CONTENT)


if __name__ == '__main__':
    unittest.main(verbosity=2)
