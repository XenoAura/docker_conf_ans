# Ansible-confluence


```bash
cd mtc_test_task/ansible_docker_confluence/
```

Запуск установки на localhost:

```bash
ansible-playbook -i hosts -l localhost web.yml

```

Запуск установки на хост из inventory:

```bash
ansible-playbook -i hosts -l centos.yandex web.yml

```


##### Playbook делает следующие вещи:
- устанавливает docker
- устанавливает java 8
- устанавливает postgresql
- создает базу данных для confluence
- создает пользователя базы данных для confluence
- скачивает и устанавливает Atlassian Confluence v6.13.0
- добавляет Confluence в автозапуск systemd
- устанавливает необходимые для тестов пакеты(python-pip, requests)


Логирование, доступ по ipv4, а так же настройки веб сервера proxyName, proxyPort, sheme, задаются из файлов-шаблонов **roles/confluence/templates**:
- server.xml.j2
- setenv.sh.j2
- web.xml.j2

#### Тестирование rest api confluence:


Запуск тестов:

```bash
cd mtc_test_task/
python -m unittest -v confluence_rest_tests 
```

Список тестов:
1. Аутентификация
2. Создание пространства(space)
3. Создание страницы
4. Получение страницы
5. Модификация страницы(изменение контента)
6. Удаление страницы

#### Просмотр статистики запросов:

``` bash
cd /var/lib/confluence/logs
```

по количеству запросов по дате(последние 10 дней)
``` bash
grep -E 'AccessLogFilter.*rest' atlassian-confluence.log | awk '{print $1, $8}' | sort | uniq -c| sort -t'-' -k2 | head -n 10
``` 

все запросы к api:
``` bash
grep -E 'AccessLogFilter.*api' atlassian-confluence.log
``` 

всего запросов к api:
``` bash
grep -E 'AccessLogFilter.*api' atlassian-confluence.log | wc -l
``` 

по количеству запросов по каждому url:
``` bash
grep -E 'AccessLogFilter.*api' atlassian-confluence.log | awk '{print $9}' | sort | uniq -c| sort -nr| head -n 10
``` 

по кол-ву посещений и типу запроса, GET\POST\PUT\DELETE
 ``` bash
 grep -E 'AccessLogFilter.*rest' atlassian-confluence.log | awk '{print $1, $8}' | sort | uniq -c| sort -nr | head -n 10
 ```
 
 

#### Логирование

В файле **log4j.properties** раскомментирована строка:

**log4j.category.com.atlassian.confluence.util.AccessLogFilter=INFO**


В файл **web.xml** добавлен фильтр:
```XML
    <filter-mapping>
        <filter-name>AccessLogFilter</filter-name>
        <url-pattern>/rest/*</url-pattern>
    </filter-mapping>
``` 

Все запросы к статистике грепаются по фильтру AccessLogFilter.


##### Прочее

Включение ipv4.

В файл **setenv.sh** добавлена опция:

**CATALINA_OPTS="-Djava.net.preferIPv4Stack=true ${CATALINA_OPTS}"**


##### TODO

1. Запуск от пользователя
2. Reverse proxy
3. Разобраться с JAVA_HOME
4. Запуск тестов с помощью ansible
5. Автоматическая настройка базы и ключа лицензии?
